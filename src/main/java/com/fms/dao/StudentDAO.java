package com.fms.dao;

import java.util.List;

import com.fms.model.Student;


public interface StudentDAO {
	
	 List<Student> get();
	 Student get(int id);
	 void registerStudent(Student user);
	 void deleteStudent(int id);
	 String login(Student user);

}
