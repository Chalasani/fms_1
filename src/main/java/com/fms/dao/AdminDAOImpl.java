package com.fms.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.*;

import com.fms.model.Product;
import com.fms.model.Student;
import com.fms.model.User;

import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Student> allStudents() {
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from Student";
		Query<Student> query =currentSession.createQuery(s,Student.class);
	 List<Student> students= query.getResultList();
		return students;
	}
	@Override
	public void updateStudent(Student student) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.update(student);
	}

}
