package com.fms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.fms.dao.StudentDAO;
import com.fms.model.Student;

@Service
public class StudentServiceImpl implements StudentService {

	
	@Autowired
	private StudentDAO studentDAO;
	
	@Transactional
	@Override
	public List<Student> get() {
		return studentDAO.get();
	}

	@Transactional
	@Override
	public Student get(int id) {
		   
		return studentDAO.get(id);
	}
	@Transactional
	@Override
	public void registerStudent(Student student) {
		// TODO Auto-generated method stub
		 studentDAO.registerStudent(student);
		
	}
	
	@Transactional
	@Override
	public void deleteStudent(int id) {
		System.out.println("ID"+id);
		// TODO Auto-generated method stub
		studentDAO.deleteStudent(id);
		
	}

	@Override
	public String login(Student student) {
		// TODO Auto-generated method stub
		 return studentDAO.login(student);
		
	}

}
