package com.fms.service;

import java.util.List;

import com.fms.model.Student;
import com.fms.model.User;

public interface StudentService {

	 List<Student> get();
	 Student get(int id);
	 void registerStudent(Student user);
	 void deleteStudent(int id);
	 String login(Student user);
}
