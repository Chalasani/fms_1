package com.fms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.fms.dao.AdminDAO;
import com.fms.model.Student;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDAO adminDAO;
	
	@Override
	public List<Student> allStudents(){
		
		return adminDAO.allStudents();
	}

	@Transactional
	@Override
	public void updateStudent(Student student) {
		adminDAO.updateStudent(student);
		
	}

}
