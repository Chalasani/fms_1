if(localStorage.getItem("admin") == null){
    window.location.href="../home/index.jsp";
}
document.getElementById("welcome").innerHTML="Hello, "+ localStorage.getItem('admin');

function logout(){
 localStorage.removeItem('admin');
 window.location.href="../home/index.jsp";

}

var allStudents;
getAllStudents();
function getAllStudents(){
    var xmlHttpRequest= new XMLHttpRequest();
    xmlHttpRequest.open('get', "http://localhost:12096/api/allStudents");
    xmlHttpRequest.setRequestHeader('Content-type', 'application/json');
    xmlHttpRequest.send();
       xmlHttpRequest.onreadystatechange= function(){
           if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){
                console.log('responseText', this.responseText);
                 allStudents= JSON.parse(this.responseText);
                console.log('response',allStudents);
               var students=document.getElementById('students');
                    var options="";
                allStudents.forEach(function(student){
                               if(student.result == null || student.result == "no"){
                              options+="<option>"+student.username+"</option>";
                                }
                });
                students.innerHTML=options;
                showResultsAllStudents(allStudents);
                
              }
    }
    }
    function showResultsAllStudents(allStudents){
        console.log('allStudents', allStudents);

        var tbody=document.getElementById("tbody");

        var tablebodydata="";
         for(var i=0;i<allStudents.length;i++){
              var student=allStudents[i];
              console.log('result', student.result);
                if(student.result == "true"){
           tablebodydata+= "<tr><td>"+student.username+"</td>" + "<td>"+student.department+"</td>"+ "<td>"+student.subject1+"</td>"+ "<td>"+student.result1+"</td>"+"<td>"+student.subject2+ "<td>"+student.result2+"</td>"+"<td>"+student.subject3+ "<td>"+student.result3 +"</td>"+"<td><button type='button' class='btn btn-warning'>Edit</button> </td>"+ "<td><button type='button' class='btn btn-danger'>Delete</button> </td>" +"</tr>";
                }
         }
     tbody.innerHTML=tablebodydata;

    }

    function saveResult(){
        event.preventDefault();
       var username= getValueById("students");
       var subject1 =getValueById("subject1");
       var subject2 =getValueById("subject2");
       var subject3 =getValueById("subject3");
       var result1 =getValueById("result1");
       var result2 =getValueById("result2");
       var result3 =getValueById("result3");
       var department= getValueById("department");
       
        console.log(username);
        console.log(subject1);
        console.log(subject2);
        console.log(subject3);

       var studentObject=getStudentByUserName(username);
         studentObject.subject1=subject1;
         studentObject.subject2=subject2;
         studentObject.subject3=subject3;
         studentObject.result1=result1;
         studentObject.result2=result2;
         studentObject.result3=result3;
         studentObject.result =true;
         studentObject.department=department;

         console.log(studentObject);

         var xmlHttpRequest= new XMLHttpRequest();
xmlHttpRequest.open('put', "http://localhost:12096/admin/updateResult");
xmlHttpRequest.setRequestHeader('Content-type', 'application/json');
xmlHttpRequest.send(JSON.stringify(studentObject));
   xmlHttpRequest.onreadystatechange= function(){
       if(xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200){
            console.log('responseText', this.responseText);
            var response= JSON.parse(this.responseText);
            console.log('response', response);
            if(response.result == "true"){
                getAllStudents();
            }
    }
}
    }
     function getValueById(id){
          return document.getElementById(id).value;
     }
     function getStudentByUserName(username){
            for(var i=0;i<allStudents.length;i++){
                if(allStudents[i].username == username){
                       return allStudents[i];  
                }
            }

     }
