<html>
    <head>

            <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
            
            <link rel="stylesheet" href="index.css">

    </head>

     <body>
                    <!--  Remove dark nav  -->
                    <nav class="nav dark-nav" style="background-color: #17332d">
                      <div class="container">
                        <div class="nav-heading">
                          <button class="toggle-nav" data-toggle="open-navbar1"><i class="fa fa-align-right"></i></button>
                          <a style="font-size: 30px" class="brand" href="index.jsp">FMS - Result App</a>
                        </div>
                        <div class="menu" id="open-navbar1">
                          <ul class="list">
                            <li><a href="#login" class="trigger-btn" data-toggle="modal" style="font-size: 20px">Login</a></li>
                            <li><a href="#register" class="trigger-btn" data-toggle="modal" style="font-size: 20px">Register</a></li>
                            <li><a href="#admin" class="trigger-btn" data-toggle="modal" style="font-size: 20px">Admin</a></li>
                            <li><a style="font-size: 20px" href="#">Support</a></li>
                           
                          </ul>
                        </div>
                      </div>
                    </nav>
                    <div id="login" class="modal fade">
                            <div class="modal-dialog modal-login">
                                <div class="modal-content">
                                    <form>
                                        <div class="modal-header">				
                                            <h3 class="modal-title" style="color:green">Login Here</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">				
                                            <div class="form-group">
                                                <label>username</label>
                                                <input type="text" id="student_username" class="form-control" required="required">
                                            </div>
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label>Password</label>
                                                    <a href="#" class="float-right text-muted"><small>Forgot?</small></a>
                                                </div>
                                                
                                                <input type="password" id="student_password" class="form-control" required="required">
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <label class="form-check-label"><input type="checkbox"> Remember me</label>
                                            <button onclick="login()" class="btn btn-success">Login</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>  

                        <div id="admin" class="modal fade">
                            <div class="modal-dialog modal-login">
                                <div class="modal-content">
                                    <form>
                                        <div class="modal-header">				
                                            <h3 class="modal-title" style="color:green">Admin - Login Here</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">				
                                            <div class="form-group">
                                                <label> username</label>
                                                <input type="text" id="username" class="form-control" required="required">
                                            </div>
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label>Password</label>
                                                    <a href="#" class="float-right text-muted"><small>Forgot?</small></a>
                                                </div>
                                                
                                                <input type="password" id="password" class="form-control" required="required">
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <label class="form-check-label"><input type="checkbox"> Remember me</label>
                                            <button onclick="adminLogin()" class="btn btn-success">Login</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>  
                        
                        <div id="register" class="modal fade">
                                <div class="modal-dialog modal-login">
                                    <div class="modal-content">
                                        <form>
                                            <div class="modal-header">				
                                                <h3 class="modal-title" style="color:green">Register Here</h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">				
                                                <div class="form-group">
                                                    <label>username</label>
                                                    <input type="text" id="reg_username" class="form-control" required="required">
                                                </div>
                                                <div class="form-group">
                                                        <label>emailid</label>
                                                        <input type="text" id="reg_email" class="form-control" required="required">
                                                    </div>
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <label>Password</label>
                                                        <a href="#" class="float-right text-muted"><small>Forgot?</small></a>
                                                    </div>
                                                    
                                                    <input id="reg_password" type="password" class="form-control" required="required">
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <label class="form-check-label"><input type="checkbox"> Remember me</label>
                                                <button onclick="register()" class="btn btn-success">Register</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
     <img width="100%" src="../assets/images/res.jpeg">
      <script src="index.js"></script>
     </body>
</html>
